
Branches:

jsp -> compile and generate war, deploy to tomcat, upload to nexus

docker -> compile and generate war, build docker image with Dockerfile copying the war

docker_gitlab_local -> like the docker branch but pulling from a local gitlab repo

ssh_script -> for using Jenkins for launching scripts with ssh
